﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour

{
    public float velocidadY;
    public float posY;
    public float direction;
    private Vector3 newPosition = new Vector3(0,0,0);

    private float limit = 7.5f;
 
    // Update is called once per frame
    void Update()
    {
        
        direction = Input.GetAxis("Vertical");
 
        Debug.Log(transform.position.y);
        Debug.Log(transform.rotation.y);
      
        posY = transform.position.y + direction*velocidadY*Time.deltaTime;

        if (posY <= -limit){
            posY = -limit;
        }

        if (posY >= limit){
           posY = limit;
        }

        newPosition.x = transform.position.x;
        newPosition.y = posY;
        newPosition.z = transform.position.z;
        transform.position = newPosition;  


    }
}